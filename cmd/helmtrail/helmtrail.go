package main

import (
	"fmt"
	"os"
	"os/exec"
	"path"
)

var (
	cmdName string
)

func printUsage(err error) {

	if err != nil {
		fmt.Println("ERROR: " + err.Error())
	}
	fmt.Println("Usage: " + cmdName + " <appid> <traildir> <configs-dir>")
}

func main() {

	cmdName = path.Base(os.Args[0])

	if len(os.Args) < 3 {
		printUsage(nil)
		os.Exit(1)
	}

	appName := os.Args[1]
	trailPath := os.Args[2]
	configPath := os.Args[3]

	// check trail path exist
	_, err := os.Stat(trailPath)
	if err != nil {
		printUsage(err)
		os.Exit(2)
	}

	// check app trail path exist
	appTrailPath := path.Join(trailPath, appName)
	_, err = os.Stat(appTrailPath)
	if err != nil {
		printUsage(err)
		os.Exit(3)
	}

	// check configs path exist
	_, err = os.Stat(configPath)
	if err != nil {
		printUsage(err)
		os.Exit(4)
	}

	valuesPath := path.Join(configPath, "_", "system-config.helm.yaml")
	// check values path exists; otherwise lets move on...
	_, err = os.Stat(valuesPath)
	if err != nil {
		fmt.Println("Skipping app without helm config: " + appName)
		os.Exit(0)
	}

	appHelmPath := path.Join(appTrailPath, "_helm")
	// check helm chart path exists; otherwise lets move on...
	_, err = os.Stat(appHelmPath)
	if err != nil {
		fmt.Println("Skipping app without _helm config: " + appName)
		os.Exit(0)
	}

	appHelmChartPath := path.Join(appHelmPath, "Chart.yaml")

	// check helm chart path exists; otherwise lets move on...
	_, err = os.Stat(appHelmChartPath)
	if err != nil {
		fmt.Println("App has _helm config, but no Chart.yaml: " + appName)
		os.Exit(5)
	}

	command := exec.Command("helm", "template", "--output-dir", "--values", valuesPath, appHelmPath)

	err = command.Run()

	if err != nil {
		printUsage(err)
		os.Exit(125)
	}
}
